<?php
	include 'settings.php';
	
	$path = $BROWSE_URL.$_POST['path'];
	$iterator = new \DirectoryIterator($path);

	$result = array();
	foreach ($iterator as $entry) {
		if ($entry->isDot() === false) {
			$subResult = array();
			$subResult['n'] = $entry->getBasename();
			$subResult['lm'] = $entry->getMTime();

			if ($entry->isFile()) {
				$subResult['s'] = $entry->getSize();
			} else if ($entry->isDir()) {
				$subResult['s'] = -1;
			}
			$result[] = $subResult;
		}
	}

	echo json_encode($result);
?>