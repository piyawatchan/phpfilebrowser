<?php
	include 'settings.php';

	$path = $BROWSE_URL.$_GET['path'];
	$fileName = basename($path);

	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.$fileName);

	readfile($path);
?>