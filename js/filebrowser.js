var SERVER_BROWSE = 'php/browse.php';
var SERVER_DOWNLOAD = 'php/download.php';

var KB = 1024;
var MB = KB * 1024;

var MONTH = 
['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

var MS_DAY = 1000 * 60 * 60 * 24;

var theTable;

// page start
$(function () {
	$.ajaxSetup({
		type: 'POST'
	});

	theTable = $('table.browser');
	attachEvents(theTable);

	theTable.append(TEMPLATE.spinner(0, 0));
	AJAX.browse('');
});

function attachEvents (table) {
	table.on('click', 'tr.folder div.navi-closed', EVENTS.openFolder);
	table.on('click', 'tr.folder div.navi-open', EVENTS.closeFolder);
	table.on('click', 'tr.folder div.symbol-folder, tr.folder td.name span', EVENTS.openOrCloseFolder);
	table.on('click', 'tr.file div.symbol-file, tr.file td.name span', EVENTS.downloadFile);
}

EVENTS = {
	openFolder : function (event) {
		var clicked = event.currentTarget;
		var row = $(clicked).closest('tr');

		row.after(TEMPLATE.spinner(parseInt(row.attr('lv'), 10) + 1, parseInt(row.attr('id'), 10))); 
		AJAX.browse(EVENTS._getPath(clicked));
		row.find('div.navi').removeClass('navi-closed').addClass('navi-open');
	},

	closeFolder : function (event) {
		var clicked = event.currentTarget;
		var row = $(clicked).closest('tr');

		row.nextUntil('[lv=' + row.attr('lv') + ']').remove();
		row.find('div.navi').removeClass('navi-open').addClass('navi-closed');		
	},

	openOrCloseFolder : function (event) {
		var clicked = event.currentTarget;
		var row = $(clicked).closest('tr');

		if (row.find('div.navi').hasClass('navi-closed')) EVENTS.openFolder(event);
		else EVENTS.closeFolder(event);
	},

	downloadFile : function (event) {
		var clicked = event.currentTarget;
		var path = EVENTS._getPath(clicked);

		event.preventDefault();
    	window.location.href = SERVER_DOWNLOAD + '?path=' + encodeURI(path);
	},

	// figure out the server path of where it is clicked
	_getPath : function (clicked) {
		var row = $(clicked).closest('tr');
		var path = row.find('td.name span').html();
		var parent = row.attr('pr');

		while (parseInt(parent, 10) !== 0) {
			row = theTable.find('tr#' + parent);
			path = row.find('td.name span').html() + "/" + path;
			var parent = row.attr('pr');
		}
		return path;
	}
};

AJAX = {
	browse : function (path) {
		$.ajax({
			url: SERVER_BROWSE,
			data: {"path": path},
			success: AJAX.onBrowseSuccess
		});
	},

	// read AJAX response (folder contents) and render it
	onBrowseSuccess : function (data) {
		var dir = JSON.parse(data);
		var len = dir.length;

		// render folder contents after the spinner, then remove the spinner
		var spinner = theTable.find('tr.spinner');

		if (spinner.length > 0) {
			var level = parseInt(spinner.attr('lv'), 10);
			var parent = spinner.attr('pr');
			var pointer = spinner;
			var row;
			 
			for (var i = 0; i < len; i++) {
				var isFolder = (dir[i].s < 0);
				if (isFolder) row = TEMPLATE.folder(level, parent, dir[i]);
				else row = TEMPLATE.file(level, parent, dir[i]);
				pointer.after(row);
				pointer = row;
			}
			spinner.remove();
		}
	}
}

FORMAT = {
	date : function (phpLastModified) {
		// Today, 3:23 PM
		// Yesterday, 2:07 PM
		// Oct 12, 2014, 1:39 AM

		var date = new Date(phpLastModified * 1000);
		var ms = date.getTime();

		var today = (new Date()).setHours(0, 0, 0, 0);
		var yesterday = today - MS_DAY;
		var tomorrow = today + MS_DAY;

		var dateText;
		if ((today <= ms) && (ms < tomorrow)) dateText = "Today";
		else if ((yesterday <= ms) && (ms < today)) dateText = "Yesterday";
		else {
			var y = date.getFullYear(); // 2014
			var mo = date.getMonth(); // 0-11
			var d = date.getDate(); // 1-31

			dateText = MONTH[mo] + " " + d + ", " + y;
		}
		
		var timeText;
		var h = date.getHours(); // 0-23
		var m = date.getMinutes(); // 0-59
		var pm = (h >= 12) ? 'PM' : 'AM';
		h = (h % 12 === 0) ? 12 : h % 12;
		m = (m < 10) ? '0' + m : m;
		timeText = h + ":" + m + " " + pm;

		return dateText + ", " + timeText;
	},

	fileSize : function  (size) {
		if (size < 0) return '--';
		else if (size < KB) return (size === 0) ? 'Zero Bytes' : ((size === 1) ? 'One Byte' : size + " Bytes");
		else if (size < MB) return (size / KB).toFixed(1) + ' KB';
		else return (size / MB).toFixed(1) + ' MB';
	}
};

TEMPLATE = {
	rowID : 1,

	spinner : function (indentLevel, parent) {
		var row = $('<tr>').addClass('spinner').attr('lv', indentLevel).attr('pr', parent);

		row.append(
			$('<td>').addClass('name').append(
				TEMPLATE._indent(indentLevel),
				$('<div>').addClass('icon symbol-spinner')),
			$('<td>').addClass('last-modified'),
			$('<td>').addClass('size'));
			
		return row;
	},

	folder : function (indentLevel, parent, entry, isOpen) {
		var row = $('<tr>').addClass('folder').attr('lv', indentLevel).attr('id', TEMPLATE.rowID++).attr('pr', parent);

		row.append(
			$('<td>').addClass('name').append(
				TEMPLATE._indent(indentLevel),
				$('<div>').addClass((isOpen === true) ? 'icon navi navi-open' : 'icon navi navi-closed'), 
				$('<div>').addClass('icon symbol-folder'),
				$('<span>').addClass('folder-name').html(entry.n)),
			$('<td>').addClass('last-modified').append(FORMAT.date(entry.lm)),
			$('<td>').addClass('size').append(FORMAT.fileSize(entry.s)));
			
		return row;
	},

	file : function (indentLevel, parent, entry) {
		var row = $('<tr>').addClass('file').attr('lv', indentLevel).attr('id', TEMPLATE.rowID++).attr('pr', parent);

		row.append(
			$('<td>').addClass('name').append(
				TEMPLATE._indent(indentLevel + 1),
				$('<div>').addClass('icon symbol-file'),
				$('<span>').addClass('file-name').html(entry.n)),
			$('<td>').addClass('last-modified').append(FORMAT.date(entry.lm)),
			$('<td>').addClass('size').append(FORMAT.fileSize(entry.s)));
			
		return row;
	},

	_indent : function (indentLevel) {
		var ret = "";
		for (var i = 0; i < indentLevel; i++) ret += '<div class="icon spacer"></div>';
		return ret;
	}
};